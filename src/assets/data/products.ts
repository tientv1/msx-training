export const products = [
    {
        id: 1,
        name: 'Iphone 8',
        type: 'smart-phone',
        price: 5000000,
        trademark: 'apple'
    },
    {
        id: 2,
        name: 'Asus',
        type: 'laptop',
        price: 12000000,
        trademark: 'asus'
    },
    {
        id: 3,
        name: 'LG',
        type: 'television',
        price: 24000000,
        trademark: 'lg'
    },
    {
        id: 4,
        name: 'Samsung s9',
        type: 'smart-phone',
        price: 12000000,
        trademark: 'samsung'
    },
    {
        id: 5,
        name: 'Lenovo',
        type: 'laptop',
        price: 14000000,
        trademark: 'lenovo'
    },
    {
        id: 6,
        name: 'Iphone x',
        type: 'smart-phone',
        price: 23000000,
        trademark: 'apple'
    }
];
