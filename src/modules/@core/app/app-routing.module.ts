import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from 'src/modules/shares/layout/main-layout/main-layout.component';
import { AuthLayoutComponent } from 'src/modules/shares/layout/auth-layout/auth-layout/auth-layout.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'auth/login',
    pathMatch: 'full'
  },
  {
    // path: 'auth',
    // component: AuthLayoutComponent,
    // children: [
    // {
    path: 'auth',
    loadChildren: () => import('src/modules/@core/auth/auth.module').then(m => m.AuthModule)
    //   }
    // ]
  },
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: 'style',
        loadChildren: () => import('src/modules/style/style.module').then(m => m.StyleModule)
      },
      {
        path: 'user',
        loadChildren: () => import('src/modules/user/user.module').then(m => m.UserModule)
      },
      {
        path: 'product',
        loadChildren: () => import('src/modules/product/product.module').then(m => m.ProductModule)
      },
      {
        path: 'user',
        loadChildren: () => import('src/modules/user/user.module').then(m => m.UserModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
