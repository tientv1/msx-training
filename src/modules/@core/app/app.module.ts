import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxsModule } from '@ngxs/store';
import { HttpClientModule } from '@angular/common/http';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { DFNgZorroAntdModule } from 'src/modules/shares/module/ng-zorro-antd.module';
import { MainLayoutModule } from 'src/modules/shares/layout/main-layout/main-layout.module';
import { UserService } from 'src/modules/user/service/user.service';
import { UserSelector } from 'src/modules/user/state/user.selector';
import { AuthLayoutModule } from 'src/modules/shares/layout/auth-layout/auth-layout.module';

import { NzIconModule } from 'ng-zorro-antd/icon';

import { IconDefinition } from '@ant-design/icons-angular';

// import { AccountBookFill, AlertFill, AlertOutline } from '@ant-design/icons-angular/icons';

// const icons: IconDefinition[] = [ AccountBookFill, AlertOutline, AlertFill ];

import * as AllIcons from '@ant-design/icons-angular/icons';
import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';
import { AuthBiz } from 'src/modules/@core/auth/biz-model/auth.biz';

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => antDesignIcons[key]);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    NgxsModule.forRoot([]),
    HttpClientModule,
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NzIconModule.forRoot(icons),
    DFNgZorroAntdModule,
    MainLayoutModule,
    AuthLayoutModule
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    { provide: NZ_ICONS, useValue: icons },
    UserService,
    UserSelector,
    AuthBiz
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
