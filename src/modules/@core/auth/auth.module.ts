import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { Routes, RouterModule } from '@angular/router';
import { DFNgZorroAntdModule } from 'src/modules/shares/module/ng-zorro-antd.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthBiz } from 'src/modules/@core/auth/biz-model/auth.biz';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    DFNgZorroAntdModule,
    ReactiveFormsModule
  ]
})
export class AuthModule { }
