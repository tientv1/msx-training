import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class AuthBiz {
    constructor(private router: Router) { }

    login() {
        this.router.navigateByUrl('user');
    }

    logout() {
        this.router.navigateByUrl('');
    }
}
