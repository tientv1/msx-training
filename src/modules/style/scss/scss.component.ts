import { Component, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';


interface ParentItemData {
  key: number;
  name: string;
  platform: string;
  version: string;
  upgradeNum: number | string;
  creator: string;
  createdAt: string;
  expand: boolean;
}

interface ChildrenItemData {
  key: number;
  name: string;
  date: string;
  upgradeNum: string;
}

@Component({
  selector: 'app-scss',
  templateUrl: './scss.component.html',
  styleUrls: ['./scss.component.scss']
})
export class ScssComponent implements OnInit {
  mousemoveEvent$: any;
  listOfParentData: ParentItemData[] = [];
  listOfChildrenData: ChildrenItemData[] = [];

  listOfData = [
    {
      key: '1',
      name: 'John Brown',
      age: 32,
      address: 'New York No. 1 Lake Park'
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park'
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park'
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park'
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park'
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park'
    },
    {
      key: '2',
      name: 'Jim Green',
      age: 42,
      address: 'London No. 1 Lake Park'
    },
    {
      key: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sidney No. 1 Lake Park'
    }
  ];

  style: any = {
    width: 200,
    height: 50,
    backgroundColorOn: 'rgb(26, 156, 26)',
    backgroundColorOff: 'rgb(177, 177, 177)',
    labelOn: '',
    labelOff: ''
  };

  constructor() { }

  ngOnInit(): void {

    for (let i = 0; i < 10; ++i) {
      this.listOfParentData.push({
        key: i,
        name: 'Screem',
        platform: 'iOS',
        version: '10.3.4.5654',
        upgradeNum: 500,
        creator: 'Jack',
        createdAt: '2014-12-24 23:12:00',
        expand: false
      });
    }
    for (let i = 0; i < 3; ++i) {
      this.listOfChildrenData.push({
        key: i,
        date: '2014-12-24 23:12:00',
        name: 'This is production name',
        upgradeNum: 'Upgraded: 56'
      });
    }

    this.mousemoveEvent$ = fromEvent(document.getElementById('btn'), 'mousemove');

    this.mousemoveEvent$.subscribe((e: any) => {
      const x = e.pageX - e.target.offsetLeft;
      const y = e.pageY - e.target.offsetTop;
      e.target.style.setProperty('--x', `${x}px`);
      e.target.style.setProperty('--y', `${y}px`);
    });

    fromEvent(document.getElementById('box'), 'mousemove').subscribe((e: any) => {
      const x = e.pageX - e.target.offsetLeft;
      const y = e.pageY - e.target.offsetTop;
      document.getElementById('span').setAttribute('style', `left: ${x}px; top: ${y}px;`);
    });

    document.getElementById('box-toggle').classList.add('disabled');
  }

  changeStyleToggle() {
    const toggle = document.getElementsByClassName('toggle')[0] as HTMLElement;
    toggle.style.backgroundColor = this.style.backgroundColorOff;
    toggle.style.width = `${this.style.width}px`;
    toggle.style.height = `${this.style.height}px`;

    const bgToggleOn = document.getElementsByClassName('bgr-on')[0] as HTMLElement;
    bgToggleOn.style.backgroundColor = this.style.backgroundColorOn;

    const toggleOn = document.getElementsByClassName('toggle-on')[0] as HTMLElement;
    toggleOn.style.transform = `translate(${this.style.width - 50}px, 0)`;

  }

  disabled() {
    document.getElementById('box-toggle').classList.add('disabled');
  }

  enabled() {
    document.getElementById('box-toggle').classList.remove('disabled');
  }

  toggle() {
    document.getElementById('toggle').classList.toggle('toggle-on');
    document.getElementById('box-toggle').classList.toggle('bgr-on');
    const toggleStatus = document.getElementsByClassName('toggle-on');
    console.log(toggleStatus.length > 0);
    if (toggleStatus.length > 0) {
      document.getElementById('toggle-on-label').setAttribute('style', `display: block;`);
      document.getElementById('toggle-off-label').setAttribute('style', `display: none;`);
    } else {
      document.getElementById('toggle-on-label').setAttribute('style', `display: none;`);
      document.getElementById('toggle-off-label').setAttribute('style', `display: block;`);
    }
  }
}
