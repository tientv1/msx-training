import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScssComponent } from './scss/scss.component';
import { from } from 'rxjs';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: ScssComponent
  }
];

@NgModule({
  declarations: [
    ScssComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class StyleModule { }
