import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserBiz } from 'src/modules/user/biz.model/user.biz';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  formUser: FormGroup;
  success = false;

  constructor(private fb: FormBuilder, private userBiz: UserBiz, private router: Router) { }

  ngOnInit(): void {
    this.createForm();
    if (this.userBiz.checkMachine) {
      this.userBiz.initialStateMachine();
    }
  }

  createForm() {
    this.formUser = this.fb.group({
      fullname: [null, [Validators.required]],
      birthday: [null, [Validators.required]],
      gender: ['male', [Validators.required]],
      email: [null, [Validators.email, Validators.required]],
      phoneNumber: [null, [Validators.required]],
      phoneNumberPrefix: '+84',
      // username: '',
      // password: '',
      // role: '',
      // status: ''
    });
  }

  save() {
    const user = this.formUser.value;
    this.userBiz.create(user);
  }

  cancel() {
    this.userBiz.createCancel();
    this.router.navigateByUrl('/user');
  }
}
