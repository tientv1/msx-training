import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserBiz } from 'src/modules/user/biz.model/user.biz';
import { Router } from '@angular/router';
import { User } from 'src/modules/user/model/user.model';
import { Subscription } from 'rxjs';
import { NzModalService, NzModalRef } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, OnDestroy {
  users: User[] = [];
  subscription: Subscription = new Subscription();

  confirmModal?: NzModalRef;

  status = [];

  constructor(
    private userBiz: UserBiz,
    private router: Router,
    private modal: NzModalService
  ) { }

  ngOnInit(): void {
    if (this.userBiz.checkMachine) {
      this.userBiz.initialStateMachine();
    }
    this.subscription.add(this.userBiz.getUser$.subscribe(users => this.users = users));
  }

  new() {
    this.router.navigateByUrl('user/new');
  }

  update(user: User) {
    this.router.navigate([`user/edit/${user.id}`, user]);
    // this.router.navigateByUrl('user/edit/' + user.id);
  }

  showConfirm(id: string): void {
    this.userBiz.transitionToDelete();
    this.confirmModal = this.modal.confirm({
      nzTitle: 'Are you sure delete this user ?',
      nzOnOk: () => this.userBiz.delete(id),
      nzOnCancel: () => this.userBiz.deleteCancel()
    });
  }

  checkLoading(id: string) {
    const status = this.status.find(s => s.id === id);
    return status ? status.loading : false;
  }

  openLoading(id: string) {
    const status = this.status.find(s => s.id === id);
    if (status) {
      status.loading = true;
    } else {
      this.status.push({ id, loading: true });
    }
  }

  closeLoading(id: string) {
    const status = this.status.find(s => s.id === id);
    if (!status) { return; }
    status.loading = false;
  }

  changeStatus(user: User): void {
    const check = this.checkLoading(user.id);
    if (check) {
      return;
    }
    this.confirmModal = this.modal.confirm({
      nzTitle: 'Are you sure change status this user ?',
      nzOnOk: () => {
        this.userBiz.changeStatus$.subscribe(status => this.closeLoading(user.id));
        this.openLoading(user.id);
        this.userBiz.transitionToChangeStatus(user);
      },
      nzOnCancel: () => { }
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
