import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserBiz } from 'src/modules/user/biz.model/user.biz';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/modules/user/model/user.model';
import { isEqual } from 'lodash';
@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  formUser: FormGroup;
  user: User = new User();

  constructor(private fb: FormBuilder, private userBiz: UserBiz, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: User) => {
      this.user = params;
    });
    const id = this.route.snapshot.params.id;
    console.log(id);
    // this.userBiz.getOne(id).subscribe(u => user = u);
    this.createForm(this.user);
    if (this.userBiz.checkMachine) {
      this.userBiz.initialStateMachine();
    }
  }

  createForm(user: User) {
    this.formUser = this.fb.group({
      id: [!!user ? user.id : null],
      fullname: [!!user ? user.fullname : null, [Validators.required]],
      birthday: [!!user ? user.birthday : null, [Validators.required]],
      gender: [!!user ? user.gender : 'male', [Validators.required]],
      email: [!!user ? user.email : null, [Validators.email, Validators.required]],
      phoneNumber: [!!user ? user.phoneNumber : null, [Validators.required]],
      phoneNumberPrefix: [!!user ? user.phoneNumberPrefix : null, [Validators.required]],
      // username: '',
      // password: '',
      // role: '',
      status: user.status
    });
  }

  save() {
    const user = this.formUser.value;
    this.userBiz.update(user);
  }

  cancel() {
    this.userBiz.updateCancel();
    this.router.navigateByUrl('/user');
  }

  get isChange() {
    const user = this.formUser.value;
    console.log(isEqual(user, this.user));
    return isEqual(user, this.user);
  }
}
