import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { User } from 'src/modules/user/model/user.model';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {
    url: string;
    constructor(private http: HttpClient) {
        this.url = environment.URL + '/user';
    }

    fetch(): Observable<User[]> {
        return this.http.get<User[]>(this.url);
    }

    getOne(id: string): Observable<User> {
        return this.http.get<User>(`${this.url}/${id}`);
    }

    add(user: User): Observable<User> {
        return this.http.post<User>(this.url, user);
    }

    update(user: User): Observable<User> {
        return this.http.put<User>(`${this.url}/${user.id}`, user);
    }

    delete(userId: string): Observable<User> {
        return this.http.delete<User>(`${this.url}/${userId}`);
    }

    activeUser(userId: string): Observable<any> {
        return this.http.patch<any>(`${this.url}/${userId}/active`, {});
    }

    inactiveUser(userId: string): Observable<any> {
        return this.http.patch<any>(`${this.url}/${userId}/inactive`, {});
    }
}
