import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './page/user.component';
import { Routes, RouterModule } from '@angular/router';
import { AddUserComponent } from './page/add-user/add-user.component';
import { UserBiz } from './biz.model/user.biz';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NzTableModule } from 'ng-zorro-antd/table';

import { IconDefinition } from '@ant-design/icons-angular';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzRadioModule } from 'ng-zorro-antd/radio';
// import { AccountBookFill, AlertFill, AlertOutline } from '@ant-design/icons-angular/icons';

// const icons: IconDefinition[] = [ AccountBookFill, AlertOutline, AlertFill ];

import * as AllIcons from '@ant-design/icons-angular/icons';
import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { NZ_I18N, en_US } from 'ng-zorro-antd/i18n';
import { EditUserComponent } from './page/edit-user/edit-user.component';
import { DFNgZorroAntdModule } from 'src/modules/shares/module/ng-zorro-antd.module';
import { NgxsModule } from '@ngxs/store';
import { UserState } from 'src/modules/user/state/user.state';

const antDesignIcons = AllIcons as {
  [key: string]: IconDefinition;
};
const icons: IconDefinition[] = Object.keys(antDesignIcons).map(key => antDesignIcons[key]);

const routes: Routes = [
  { path: '', component: UserComponent },
  { path: 'new', component: AddUserComponent },
  { path: 'edit/:id', component: EditUserComponent },
];

@NgModule({
  declarations: [UserComponent, AddUserComponent, EditUserComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    NzTableModule,
    NzIconModule.forRoot(icons),
    DFNgZorroAntdModule,
    NgxsModule.forFeature([UserState])
  ],
  providers: [
    UserBiz,
    { provide: NZ_I18N, useValue: en_US },
    { provide: NZ_ICONS, useValue: icons }
  ]
})
export class UserModule { }
