import { Injectable } from '@angular/core';
import { UserStateModel, UserState } from './user.state';
import { Selector } from '@ngxs/store';

@Injectable()
export class UserSelector {
    @Selector([UserState])
    static getUser$(state: UserStateModel) {
        return state.users;
    }
}
