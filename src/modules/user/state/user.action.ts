import { User } from '../model/user.model';

export class StoreUserAction {
    static readonly type = '[User] Store User';
    constructor(public payload: User[]) { }
}

export class AddUserAction {
    static readonly type = '[User] Add User';
    constructor(public payload: User) { }
}

export class UpdateUserAction {
    static readonly type = '[User] Update User';
    constructor(public payload: User) { }
}

export class DeleteUserAction {
    static readonly type = '[User] Delete User';
    constructor(public payload: string) { }
}

export class ChangeStatusUserAction {
    static readonly type = '[User] Change Status User';
    constructor(public userId: string) { }
}
