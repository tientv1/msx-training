import { User } from '../model/user.model';
import { State, Action, StateContext } from '@ngxs/store';
import { StoreUserAction, AddUserAction, UpdateUserAction, DeleteUserAction, ChangeStatusUserAction } from './user.action';
import { cloneDeep } from 'lodash';

export interface UserStateModel {
    users: User[];
}

@State<UserStateModel>({
    name: 'UserState',
    defaults: {
        users: []
    }
})
export class UserState {

    @Action(StoreUserAction)
    storeUser(ctx: StateContext<UserStateModel>, action: StoreUserAction) {
        ctx.patchState({
            users: action.payload
        });
    }

    @Action(AddUserAction)
    addUser(ctx: StateContext<UserStateModel>, action: AddUserAction) {
        ctx.patchState({
            users: [...ctx.getState().users, action.payload]
        });
    }

    @Action(UpdateUserAction)
    updateUser(ctx: StateContext<UserStateModel>, action: UpdateUserAction) {
        const users = ctx.getState().users;
        const index = users.findIndex(p => p.id === action.payload.id);
        users[index] = cloneDeep(action.payload);
        ctx.patchState({
            users: [...users]
        });
    }

    @Action(DeleteUserAction)
    deleteUser(ctx: StateContext<UserStateModel>, action: DeleteUserAction) {
        const users = ctx.getState().users;
        const index = users.findIndex(p => p.id === action.payload);
        users.splice(index, 1);

        ctx.patchState({
            users: [...users]
        });
    }

    @Action(ChangeStatusUserAction)
    changeStatusUserAction(ctx: StateContext<UserStateModel>, action: ChangeStatusUserAction) {
        const users = ctx.getState().users;
        const index = users.findIndex(p => p.id === action.userId);
        const user = users[index];
        user.status = user.status === 'active' ? 'inactive' : 'active';

        ctx.patchState({
            users: [...users]
        });
    }

}
