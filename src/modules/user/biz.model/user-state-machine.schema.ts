import { User } from '../model/user.model';

export interface UserSchema {
    states: {
        fetching: {},
        showing: {},
        create: {},
        update: {},
        delete: {},
        change_status: {},
        create_processing: {},
        update_processing: {},
        delete_processing: {},
        final: {}
    };
}

export interface UserContext {
    status: string;
}
