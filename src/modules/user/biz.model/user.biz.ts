import { Injectable } from '@angular/core';
import { MachineOptions, AnyEventObject, Machine, interpret, assign } from 'xstate';
import {
    FetchSuccess,
    CreateSuccess,
    UpdateSuccess,
    FetchFail,
    CreateFail,
    UpdateFail,
    UserEvent,
    CreateUser,
    CreateConfirm,
    UpdateUser,
    UpdateConfirm,
    CreateCancel,
    UpdateCancel,
    DeleteConfirm,
    DeleteSuccess,
    DeleteFail,
    DeleteUser,
    DeleteCancel,
    ChangeStatus,
    ChangeStatusSuccess,
    ChangeStatusFail
} from './user-state-machine.event';
import { of, from, Observable, Subject } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { User } from '../model/user.model';
import { UserContext, UserSchema } from './user-state-machine.schema';
import { UserMachineConfig } from './user-state-machine.config';
import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Store, Select } from '@ngxs/store';
import { StoreUserAction, AddUserAction, UpdateUserAction, DeleteUserAction, ChangeStatusUserAction } from 'src/modules/user/state/user.action';
import { UserService } from 'src/modules/user/service/user.service';
import { UserSelector } from 'src/modules/user/state/user.selector';

@Injectable()
export class UserBiz {
    @Select(UserSelector.getUser$) readonly getUser$: Observable<User[]>;

    private machine;
    private service;
    public changeStatus$ = new Subject<boolean>();

    constructor(
        private router: Router,
        private notification: NzNotificationService,
        private store: Store,
        private userService: UserService
    ) { }

    private userStateMachineOptions: Partial<MachineOptions<UserContext, UserEvent>> = {
        actions: {
            logMessage: (_, event: AnyEventObject) => {
                console.log(event.msg);
            },
            storeUser: (_, event: FetchSuccess) => {
                this.store.dispatch(new StoreUserAction(event.users));
            },
            createUser: (_, event: CreateSuccess) => {
                this.store.dispatch(new AddUserAction(event.user));
                this.router.navigateByUrl('/user');
            },
            updateUser: (_, event: UpdateSuccess) => {
                this.store.dispatch(new UpdateUserAction(event.user));
                this.router.navigateByUrl('/user');
            },
            deleteUser: (_, event: DeleteSuccess) => {
                this.store.dispatch(new DeleteUserAction(event.userId));
            },
            changeStatus: (_, event: ChangeStatusSuccess) => {
                console.log(event.msg);
                this.store.dispatch(new ChangeStatusUserAction(event.userId));
            },
            assignStatus: assign<UserContext, UserEvent>((_, event: ChangeStatus) => {
                console.log('change_status_asign');
                return {
                    status: event.user.status
                };
            }),
        },
        services: {
            fetchUser: () => this.userService.fetch().pipe(
                map(users => {
                    return new FetchSuccess(users, 'fetch success');
                }),
                catchError(() => {
                    return of(new FetchFail('fetch fail'));
                })
            ),
            createUser: (_, event: CreateConfirm) => (this.userService.add(event.user).pipe(
                map(user => {
                    this.createNotification('success', 'Notification', 'Create User Success');
                    return new CreateSuccess(user, 'create success');
                }), catchError(() => {
                    this.createNotification('error', 'Notification', 'Create User Fail');
                    return of(new CreateFail('create fail'));
                })
            )),
            updateUser: (_, event: UpdateConfirm) => (this.userService.update(event.user).pipe(
                map(user => {
                    this.createNotification('success', 'Notification', 'Update User Success');
                    return new UpdateSuccess(user, 'update success');
                }), catchError(() => {
                    this.createNotification('error', 'Notification', 'Update User Fail');
                    return of(new UpdateFail('update fail'));
                })
            )),
            deleteUser: (_, event: DeleteConfirm) => (this.userService.delete(event.userId).pipe(
                map(() => {
                    this.createNotification('success', 'Notification', 'Delete User Success');
                    return new DeleteSuccess(event.userId, 'delete success');
                }),
                catchError(() => {
                    this.createNotification('error', 'Notification', 'Delete User Fail');
                    return of(new DeleteFail('delete fail'));
                })
            )),
            changeStatus: (_, event: ChangeStatus) => _.status !== 'active' ?
                this.userService.activeUser(event.user.id).pipe(
                    map(() => {
                        this.createNotification('success', 'Notification', 'Active User Success');
                        this.changeStatus$.next(true);
                        return new ChangeStatusSuccess(event.user.id, 'active success');
                    }),
                    catchError(() => {
                        this.createNotification('error', 'Notification', 'Active User Fail');
                        this.changeStatus$.next(false);
                        return of(new ChangeStatusFail('active fail'));
                    })
                ) : this.userService.inactiveUser(event.user.id).pipe(
                    map(() => {
                        this.createNotification('success', 'Notification', 'Inactive User Success');
                        this.changeStatus$.next(true);
                        return new ChangeStatusSuccess(event.user.id, 'inactive success');
                    }),
                    catchError(() => {
                        this.createNotification('error', 'Notification', 'Inactive User Fail');
                        this.changeStatus$.next(true);
                        return of(new ChangeStatusFail('inactive fail'));
                    })
                )
        }

    };

    initialStateMachine() {
        this.machine = Machine<UserContext, UserSchema, UserEvent>(UserMachineConfig)
            .withConfig(this.userStateMachineOptions);
        this.service = interpret(this.machine)
            .start();

        const state$ = from(this.service);
        state$.subscribe((state: any) => {
            console.log(`---------------------`);
            console.log(`Current state: ${state.value}`);
            console.log(`Available events: ${state.nextEvents}`);
            console.log(`---------------------`);
        });
    }

    transition(event: UserEvent) {
        this.service.send(event);
    }

    stopMachine() {
        this.service.stop();
    }

    transitionToCreate() {
        this.transition(new CreateUser());
    }

    create(user: User) {
        this.transitionToCreate();
        this.transition(new CreateConfirm(user));
    }

    createCancel() {
        this.transitionToCreate();
        this.transition(new CreateCancel());
    }

    transitionToUpdate() {
        this.transition(new UpdateUser());
    }

    update(user: User) {
        this.transitionToUpdate();
        this.transition(new UpdateConfirm(user));
    }

    updateCancel() {
        this.transitionToUpdate();
        this.transition(new UpdateCancel());
    }

    transitionToDelete() {
        this.transition(new DeleteUser());
    }

    delete(userId: string) {
        this.transition(new DeleteConfirm(userId));
    }

    deleteCancel() {
        this.transition(new DeleteCancel());
    }

    transitionToChangeStatus(user: User) {
        this.transition(new ChangeStatus(user));
    }

    createNotification(type: string, title: string, content: string): void {
        this.notification.create(
            type,
            title,
            content
        );
    }

    get checkMachine() {
        return !this.machine;
    }

    getOne(id: string) {
        return this.userService.getOne(id);
    }
}
