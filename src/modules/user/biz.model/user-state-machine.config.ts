import { UserContext, UserSchema } from './user-state-machine.schema';
import { MachineConfig } from 'xstate';
import {
    UserEvent,
    FetchSuccess,
    FetchFail,
    CreateUser,
    UpdateUser,
    CreateConfirm,
    CreateCancel,
    UpdateConfirm,
    UpdateCancel,
    CreateSuccess,
    CreateFail,
    UpdateSuccess,
    UpdateFail,
    DeleteConfirm,
    DeleteCancel,
    DeleteSuccess,
    DeleteFail,
    DeleteUser,
    ChangeStatusSuccess,
    ChangeStatusFail,
    ChangeStatus
} from './user-state-machine.event';

export const context: UserContext = {
    status: null
};

export const UserMachineConfig: MachineConfig<UserContext, UserSchema, UserEvent> = {
    id: 'state_machine_user',
    initial: 'fetching',
    context,
    states: {
        fetching: {
            invoke: {
                id: 'fetchUser',
                src: 'fetchUser'
            },
            on: {
                [FetchSuccess.type]: {
                    target: 'showing',
                    actions: ['storeUser', 'logMessage']
                },
                [FetchFail.type]: {
                    target: 'final',
                    actions: ['logMessage']
                },
            }
        },
        showing: {
            on: {
                [CreateUser.type]: 'create',
                [UpdateUser.type]: 'update',
                [DeleteUser.type]: 'delete',
                [ChangeStatus.type]: 'change_status'
            }
        },
        create: {
            on: {
                [CreateConfirm.type]: 'create_processing',
                [CreateCancel.type]: 'showing'
            }
        },
        create_processing: {
            invoke: {
                id: 'createUser',
                src: 'createUser'
            },
            on: {
                [CreateSuccess.type]: {
                    target: 'showing',
                    actions: ['createUser', 'logMessage']
                },
                [CreateFail.type]: {
                    target: 'create',
                    actions: ['logMessage']
                }
            }
        },
        update: {
            on: {
                [UpdateConfirm.type]: 'update_processing',
                [UpdateCancel.type]: 'showing'
            }
        },
        update_processing: {
            invoke: {
                id: 'updateUser',
                src: 'updateUser'
            },
            on: {
                [UpdateSuccess.type]: {
                    target: 'showing',
                    actions: ['updateUser', 'logMessage']
                },
                [UpdateFail.type]: {
                    target: 'update',
                    actions: ['logMessage']
                }
            }
        },
        delete: {
            on: {
                [DeleteConfirm.type]: 'delete_processing',
                [DeleteCancel.type]: 'showing'
            }
        },
        delete_processing: {
            invoke: {
                id: 'deleteUser',
                src: 'deleteUser'
            },
            on: {
                [DeleteSuccess.type]: {
                    target: 'showing',
                    actions: ['deleteUser', 'logMessage']
                },
                [DeleteFail.type]: {
                    target: 'showing',
                    actions: ['logMessage']
                }
            }
        },
        change_status: {
            entry: ['assignStatus'],
            invoke: {
                id: 'changeStatus',
                src: 'changeStatus'
            },
            on: {
                [ChangeStatusSuccess.type]: {
                    target: 'showing',
                    actions: ['changeStatus', 'logMessage']
                },
                [ChangeStatusFail.type]: {
                    target: 'showing',
                    actions: ['logMessage']
                },
            }
        },
        final: {
            type: 'final'
        }
    }
};
