import { User } from '../model/user.model';
import { EventObject } from 'xstate';

export class FetchSuccess implements EventObject {
    static readonly type = '[USER] Fetch Success';
    readonly type = FetchSuccess.type;
    constructor(public users: User[], public msg: string) { }
}

export class FetchFail implements EventObject {
    static readonly type = '[USER] Fetch Fail';
    readonly type = FetchFail.type;
    constructor(public msg: string) { }
}

export class CreateUser implements EventObject {
    static readonly type = '[USER] Create User';
    readonly type = CreateUser.type;
    constructor() { }
}

export class CreateConfirm implements EventObject {
    static readonly type = '[USER] Create Confirm';
    readonly type = CreateConfirm.type;
    constructor(public user: User) { }
}

export class CreateCancel implements EventObject {
    static readonly type = '[USER] Create Cancel';
    readonly type = CreateCancel.type;
    constructor() { }
}

export class CreateSuccess implements EventObject {
    static readonly type = '[USER] Create Success';
    readonly type = CreateSuccess.type;
    constructor(public user: User, public msg: string) { }
}

export class CreateFail implements EventObject {
    static readonly type = '[USER] Create Fail';
    readonly type = CreateFail.type;
    constructor(public msg: string) { }
}

export class UpdateUser implements EventObject {
    static readonly type = '[USER] Update User';
    readonly type = UpdateUser.type;
    constructor() { }
}

export class UpdateConfirm implements EventObject {
    static readonly type = '[USER] Update Confirm';
    readonly type = UpdateConfirm.type;
    constructor(public user: User) { }
}

export class UpdateCancel implements EventObject {
    static readonly type = '[USER] Update Cancel';
    readonly type = UpdateCancel.type;
    constructor() { }
}

export class UpdateSuccess implements EventObject {
    static readonly type = '[USER] Update Success';
    readonly type = UpdateSuccess.type;
    constructor(public user: User, public msg: string) { }
}

export class UpdateFail implements EventObject {
    static readonly type = '[USER] Update Fail';
    readonly type = UpdateFail.type;
    constructor(public msg: string) { }
}


export class DeleteUser implements EventObject {
    static readonly type = '[USER] Delete User';
    readonly type = DeleteUser.type;
    constructor() { }
}

export class DeleteConfirm implements EventObject {
    static readonly type = '[USER] Delete Confirm';
    readonly type = DeleteConfirm.type;
    constructor(public userId: string) { }
}

export class DeleteCancel implements EventObject {
    static readonly type = '[USER] Delete Cancel';
    readonly type = DeleteCancel.type;
    constructor() { }
}

export class DeleteSuccess implements EventObject {
    static readonly type = '[USER] Delete Success';
    readonly type = DeleteSuccess.type;
    constructor(public userId: string, public msg: string) { }
}

export class DeleteFail implements EventObject {
    static readonly type = '[USER] Delete Fail';
    readonly type = DeleteFail.type;
    constructor(public msg: string) { }
}

export class ChangeStatus implements EventObject {
    static readonly type = '[USER] Change Status';
    readonly type = ChangeStatus.type;
    constructor(public user: User) { }
}

export class ChangeStatusSuccess implements EventObject {
    static readonly type = '[USER] Change Status Success';
    readonly type = ChangeStatusSuccess.type;
    constructor(public userId: string, public msg: string) { }
}

export class ChangeStatusFail implements EventObject {
    static readonly type = '[USER] Change Status Fail';
    readonly type = ChangeStatusFail.type;
    constructor(public msg: string) { }
}

export type UserEvent = FetchSuccess | FetchFail
    | CreateUser | CreateSuccess | CreateConfirm | CreateCancel | CreateFail
    | UpdateUser | UpdateCancel | UpdateConfirm | UpdateSuccess | UpdateFail
    | DeleteUser | DeleteConfirm | DeleteCancel | DeleteSuccess | DeleteFail
    | ChangeStatus | ChangeStatusSuccess | ChangeStatusFail;
