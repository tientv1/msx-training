import { UserRoleEnum } from '../../shares/enum/roles.enum';

export class User {
    id: string;
    fullname: string;
    birthday: string;
    gender: string;
    email: string;
    phoneNumberPrefix: string;
    phoneNumber: string;
    // username: string;
    // password: string;
    // role: UserRoleEnum;
    status: string;
}
