import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ScheduleComponent } from 'src/modules/schedule-vilimo/page/schedule/schedule.component';
import { ScheduleV2Component } from './page/schedule-v2/schedule-v2.component';

@NgModule({
  declarations: [ScheduleComponent, ScheduleV2Component],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: ScheduleComponent },
      { path: 'v2', component: ScheduleV2Component },
    ]),
    ReactiveFormsModule,
    FormsModule
  ]
})
export class ScheduleVilimoModule { }
