import { Component, OnInit } from '@angular/core';
import { cloneDeep } from 'lodash';
import { Guid } from 'guid-typescript';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {

  timelines: any[] = [];
  time: Date = null;
  startTime: Date = this.timeConvert(360);
  endTime: Date = this.timeConvert(720);
  selectStation = 0;
  moveTime = 60;
  repeatTime = 60;
  breakTime = 0;

  sts: any[] = [];
  schedules: any[] = [];

  select = '';

  stations = [{
    id: Guid.create().toString(),
    name: 'Bến xe A',
    totalVehicle: 10,
    totalDriver: 10,
    totalVehicleBackup: 3
  },
  {
    id: Guid.create().toString(),
    name: 'Bến xe B',
    totalVehicle: 10,
    totalDriver: 10,
    totalVehicleBackup: 3
  }
  ];

  levels = [
    {
      label: 'Ít',
      value: 'small'
    },
    {
      label: 'Vừa',
      value: 'medium'
    },
    {
      label: 'Nhiều',
      value: 'large'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

  setTimeLine() {
    this.timelines = [[], []];
    let time = this.startTime;
    do {
      this.timelines.forEach(t => t.push({
        label: time,
        value: 'small'
      }));
      time = this.sumTime(time, this.timeConvert(this.repeatTime));
    } while (time.getTime() <= this.endTime.getTime());
  }

  timeConvert(time) {
    const minute = time;
    const hours = Math.floor(minute / 60);
    const minutes = minute - hours * 60;
    const timeConvert = new Date(new Date().setHours(hours, minutes, 0, 0));
    return timeConvert;
    // this.setTimeLine(time);
  }


  sumTime(origin: Date, time: Date) {
    return new Date(new Date().setHours((origin.getHours() + time.getHours()), (origin.getMinutes() + time.getMinutes()), 0, 0));
  }

  changeStation(index) {
    this.selectStation = index;
  }

  createSchedule() {
    this.sts = [];
    this.stations.forEach(s => {
      this.sts.push({
        id: Guid.create().toString(),
        ...s,
        drivers: this.createDriver(s.totalDriver, s.name),
        vehicles: this.createVehicle(s.totalVehicle, s.name),
        vehicleBackup: this.createVehicleBackup(s.totalVehicleBackup, s.name)
      });
    });
    // console.log(this.sts);
    this.schedules = this.getSchedule();
    // console.log(this.schedules);
  }

  getSchedule() {
    const schedules = [[], []];
    const timeLineConvert = this.timeLineConvert();
    console.log(timeLineConvert);
    timeLineConvert.forEach(timeline => timeline.forEach((t, index) => {
      // console.log(index);
      // console.log(schedules[index]);
      switch (t.value) {
        case 'small': { schedules[index] = this.createTrip(schedules[index], index, 1, t.label); break; }
        case 'medium': { schedules[index] = this.createTrip(schedules[index], index, 3, t.label); break; }
        case 'large': { schedules[index] = this.createTrip(schedules[index], index, 6, t.label); break; }
        default: console.log('fail');
      }
    }));
    return schedules;
  }

  timeLineConvert() {
    const timeLineConvert = [];
    this.timelines[0].forEach(t => {
      timeLineConvert.push([this.getTime(0, t.label), this.getTime(1, t.label)]);
    });
    return timeLineConvert;
  }

  getTime(index, time) {
    return this.timelines[index].find(t => t.label === time);
  }

  createTrip(schedules, i, totalVehicle, time) {
    let index = 1;
    do {
      schedules.push({
        id: Guid.create().toString(),
        time,
        driver: this.getDriver(time, i),
        vehicle: this.getVehicle(time, i)
      });
      index++;
    } while (index <= totalVehicle);
    return schedules;
  }

  getDriver(time, index) {
    // console.log(index)
    let driver;
    if (!this.sts || !this.sts[index].drivers) {
      return null;
    }
    const drivers = this.sts[index].drivers.filter(d => new Date(d.endTime).getTime() <= new Date(time).getTime()
      || d.endTime === null
    );
    if (!drivers || drivers.length === 0) { return null; }
    driver = cloneDeep(drivers[0]);
    // console.log(driver)
    this.sts[index].drivers = this.sts[index].drivers.filter(d => d.id !== driver.id);
    driver.startTime = time;
    driver.endTime = this.sumTime(time, this.timeConvert((this.moveTime + this.breakTime)));
    if (index === 1) {
      this.sts[0].drivers.push(driver);
    } else {
      this.sts[1].drivers.push(driver);
    }
    return driver;
  }

  getVehicle(time, stationIndex) {
    let vehicle;
    if (!this.sts || !this.sts[stationIndex].drivers) {
      return null;
    }
    const vehicles = this.sts[stationIndex].vehicles.filter(d => new Date(d.endTime).getTime() <= new Date(time).getTime()
      || d.endTime === null);
    if (!vehicles || vehicles.length === 0) {
      if (!this.sts[stationIndex].vehicleBackup.length || this.sts[stationIndex].vehicleBackup.length === 0) {
        return null;
      }
      vehicle = cloneDeep(this.sts[stationIndex].vehicleBackup[0]);
      if (!vehicle) {
        return null;
      }
      this.sts[stationIndex].vehicleBackup = this.sts[stationIndex].vehicleBackup.filter(v => v.id !== vehicle.id);
    } else {
      vehicle = cloneDeep(vehicles[0]);
      // console.log(vehicle)
      if (!vehicle) {
        return null;
      }
      this.sts[stationIndex].vehicles = this.sts[stationIndex].vehicles.filter(v => v.id !== vehicle.id);
    }
    vehicle.startTime = time;
    vehicle.endTime = this.sumTime(time, this.timeConvert((this.moveTime + this.breakTime)));
    // console.log(vehicle)
    if (stationIndex === 1) {
      this.sts[0].vehicles.push(vehicle);
    } else {
      this.sts[1].vehicles.push(vehicle);
    }
    return vehicle;
  }

  createDriver(total, stationName) {
    const drivers = [];
    let index = 1;
    do {
      drivers.push({
        id: Guid.create().toString(),
        name: `Tài xế ${index} - ${stationName}`,
        startTime: null,
        endTime: null
      });
      index++;
    } while (index <= total);
    return drivers;
  }

  createVehicle(total, stationName) {
    const vehicles = [];
    let index = 1;
    do {
      vehicles.push({
        id: Guid.create().toString(),
        name: `Xe ${index} - ${stationName}`,
        startTime: null,
        endTime: null,
        status: 'active'
      });
      index++;
    } while (index <= total);
    return vehicles;
  }

  createVehicleBackup(total, stationName) {
    const vehicles = [];
    let index = 1;
    do {
      vehicles.push({
        id: Guid.create().toString(),
        name: `Xe dự phòng ${index} - ${stationName}`,
        startTime: null,
        endTime: null,
        status: 'active'
      });
      index++;
    } while (index <= total);
    return vehicles;
  }

  getScheduleOfStation(time, stationIndex) {
    if (!this.schedules) {
      return [];
    }
    const schedules = cloneDeep(this.schedules[stationIndex]);
    if (!schedules) {
      return [];
    }
    const ss = schedules.filter(s => new Date(s.time).getHours() === new Date(time).getHours() &&
      new Date(s.time).getMinutes() === new Date(time).getMinutes()
    );
    // console.log(ss)
    return ss ? ss : [];
  }

  setSelect(id) {
    if (!id) {
      return;
    }
    this.select = id;
  }

  clearSelect(event) {
    console.log('dblclick')
    this.select = '';
  }
}
