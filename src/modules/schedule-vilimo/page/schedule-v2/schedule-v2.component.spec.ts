import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleV2Component } from './schedule-v2.component';

describe('ScheduleV2Component', () => {
  let component: ScheduleV2Component;
  let fixture: ComponentFixture<ScheduleV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScheduleV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
