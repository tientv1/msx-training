import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthBiz } from 'src/modules/@core/auth/biz-model/auth.biz';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor(private router: Router, private authBiz: AuthBiz) { }

  ngOnInit(): void {
  }

  nextPage(page: string) {
    this.router.navigateByUrl(page);
  }

  logout() {
    this.authBiz.logout();
  }
}
