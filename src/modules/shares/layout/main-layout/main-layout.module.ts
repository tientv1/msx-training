import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutComponent } from './main-layout.component';
import { RouterModule } from '@angular/router';
import { DFNgZorroAntdModule } from 'src/modules/shares/module/ng-zorro-antd.module';

@NgModule({
  declarations: [MainLayoutComponent],
  imports: [
    CommonModule,
    DFNgZorroAntdModule,
    RouterModule
  ],
  exports: [MainLayoutComponent]
})
export class MainLayoutModule { }
