import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeachComponent } from './page/seach/seach.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [SeachComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: SeachComponent }
    ]),
    ReactiveFormsModule,
    FormsModule
  ]
})
export class SeachModule { }
