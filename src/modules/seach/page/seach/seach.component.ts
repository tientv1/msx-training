import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { fromEvent, from, of } from 'rxjs';
import { debounceTime, map, mergeMap } from 'rxjs/operators';
import { products } from 'src/assets/data/products';

@Component({
  selector: 'app-seach',
  templateUrl: './seach.component.html',
  styleUrls: ['./seach.component.scss']
})
export class SeachComponent implements OnInit {

  formSeach: FormGroup;
  trademarks = [
    {
      label: 'Apple',
      value: 'apple'
    },
    {
      label: 'LG',
      value: 'lg'
    },
    {
      label: 'Lenovo',
      value: 'lenovo'
    },
    {
      label: 'Asus',
      value: 'asus'
    },
    {
      label: 'Samsung',
      value: 'samsung'
    }
  ];

  types = [
    {
      label: 'Smartphone',
      value: 'smart-phone'
    },
    {
      label: 'Laptop',
      value: 'laptop'
    },
    {
      label: 'Tivi',
      value: 'television'
    }
  ];

  products = [];
  productFilter = [];

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.products = products;
    this.productFilter = products;
    this.createForm();
  }

  createForm() {
    this.formSeach = this.fb.group({
      name: '',
      type: '',
      trademarks: this.fb.array([])
    });

    this.trademarks.forEach(t => {
      ((this.formSeach.controls.trademarks) as FormArray).push(this.fb.group({
        name: t.label,
        value: t.value,
        status: false
      }));
    });

    this.formSeach.controls.name.valueChanges.pipe(
      mergeMap(t => {
        return of(t);
      }),
      debounceTime(500)
    ).subscribe(value => {
      this.seach();
    });

    this.formSeach.controls.type.valueChanges.pipe(
      mergeMap(t => {
        return of(t);
      }),
      debounceTime(500)
    ).subscribe(value => {
      this.filterProduct();
    });

    this.formSeach.controls.trademarks.valueChanges.pipe(
      mergeMap(t => {
        return of(t);
      }),
      debounceTime(500)
    ).subscribe(value => {
      this.filterProduct();
    });
  }

  createTrademark(t): FormGroup {
    return this.fb.group({
      name: t.label,
      value: t.value,
      status: false
    });
  }

  filterProduct() {
    this.productFilter = [];
    if (this.checkValidation()) {
      return this.productFilter = this.products;
    }
    this.products.forEach(p => {
      console.log(!!this.trademarkFormGroup.value.find(t => t.status === true));
      if (
        this.formSeach.controls.type.value === ''
        && !!this.trademarkFormGroup.value.find(t => t.status === true) === true
        && this.checkTrademark(p.trademark) === true
      ) {
        this.productFilter.push(p);
      } else if (
        this.formSeach.controls.type.value !== ''
        && p.type === this.formSeach.controls.type.value
        && !!this.trademarkFormGroup.value.find(t => t.status === true) !== true
      ) {
        this.productFilter.push(p);
      } else if (
        this.formSeach.controls.type.value !== ''
        && !!this.trademarkFormGroup.value.find(t => t.status === true) === true
        && p.type === this.formSeach.controls.type.value
        && this.checkTrademark(p.trademark) === true
      ) {
        this.productFilter.push(p);
      }
      else {
        return;
      }
    });
  }

  checkValidation() {
    const type = this.formSeach.controls.type.value;
    const isTrademark = this.trademarkFormGroup.value.find(t => t.status === true);
    return type === '' && !isTrademark;
  }

  checkTrademark(trademark) {
    const trademarks = this.trademarkFormGroup.value;
    if (!trademarks) {
      return false;
    }
    const tmk = trademarks.find(t => t.status === true && t.value === trademark);
    return !!tmk ? true : false;
  }

  get trademarkFormGroup() {
    return this.formSeach.get('trademarks') as FormArray;
  }

  seach() {
    this.productFilter = [];
    this.products.forEach(p => {
      console.log(this.checkTrademark(p.trademark));
      if ((this.formSeach.controls.name.value !== '' &&
        p.name.toUpperCase().lastIndexOf(this.formSeach.controls.name.value.toUpperCase()) !== -1)
      ) {
        this.productFilter.push(p);
      }
    });
  }

}
