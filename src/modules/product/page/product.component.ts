import { Component, OnInit, OnDestroy } from '@angular/core';
import { Product } from 'src/modules/product/model/product.model';
import { ProductBiz } from 'src/modules/product/biz-model/product.biz';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit, OnDestroy {
  products: Product[] = [];
  subscriptions: Subscription = new Subscription();

  constructor(private productBiz: ProductBiz) { }

  ngOnInit(): void {
    this.productBiz.fetch();
    this.subscriptions.add(
      this.productBiz.getProduct$.subscribe(products => this.products = products)
    );
  }

  delete(id: string) {
    this.productBiz.delete(id);
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }

}
