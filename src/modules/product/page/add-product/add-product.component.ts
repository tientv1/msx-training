import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductBiz } from 'src/modules/product/biz-model/product.biz';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {
  formProduct: FormGroup;

  trademarks = [
    {
      label: 'Apple',
      value: 'apple'
    },
    {
      label: 'LG',
      value: 'lg'
    },
    {
      label: 'Lenovo',
      value: 'lenovo'
    },
    {
      label: 'Asus',
      value: 'asus'
    },
    {
      label: 'Samsung',
      value: 'samsung'
    }
  ];

  types = [
    {
      label: 'Smartphone',
      value: 'smart-phone'
    },
    {
      label: 'Laptop',
      value: 'laptop'
    },
    {
      label: 'Tivi',
      value: 'television'
    }
  ];

  constructor(private fb: FormBuilder, private productBiz: ProductBiz) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.formProduct = this.fb.group({
      name: ['', Validators.required],
      type: ['', Validators.required],
      trademark: ['', Validators.required],
      price: [0, Validators.required]
    });
  }

  save() {
    const product = this.formProduct.value;
    this.productBiz.add(product);
  }
}
