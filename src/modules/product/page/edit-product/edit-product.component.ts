import { Component, OnInit } from '@angular/core';
import { Product } from 'src/modules/product/model/product.model';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ProductBiz } from 'src/modules/product/biz-model/product.biz';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  formProduct: FormGroup;

  trademarks = [
    {
      label: 'Apple',
      value: 'apple'
    },
    {
      label: 'LG',
      value: 'lg'
    },
    {
      label: 'Lenovo',
      value: 'lenovo'
    },
    {
      label: 'Asus',
      value: 'asus'
    },
    {
      label: 'Samsung',
      value: 'samsung'
    }
  ];

  types = [
    {
      label: 'Smartphone',
      value: 'smart-phone'
    },
    {
      label: 'Laptop',
      value: 'laptop'
    },
    {
      label: 'Tivi',
      value: 'television'
    }
  ];

  constructor(private fb: FormBuilder, private productBiz: ProductBiz, private route: ActivatedRoute) { }

  ngOnInit() {
    this.createForm();
    const id = this.route.snapshot.paramMap.get('id');
    console.log(id)
    this.productBiz.findOne(id).subscribe(product => {
      console.log(product);
      this.initialForm(product)
    }
    );
  }

  createForm() {
    this.formProduct = this.fb.group({
      name: ['', Validators.required],
      type: ['', Validators.required],
      trademark: ['', Validators.required],
      price: [0, Validators.required]
    });
  }

  initialForm(product: Product) {
    this.formProduct = this.fb.group({
      id: [product.id],
      name: [product.name, Validators.required],
      type: [product.type, Validators.required],
      trademark: [product.trademark, Validators.required],
      price: [product.price, Validators.required]
    });
  }

  save() {
    const product = this.formProduct.value;
    this.productBiz.update(product);
  }

}
