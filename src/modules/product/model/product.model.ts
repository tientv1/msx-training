export class Product {
    // tslint:disable-next-line: variable-name
    id: string;
    name: string;
    type: string;
    trademark: string;
    price: number;

    constructor() {
        this.id = '';
        this.name = '';
        this.type = '';
        this.trademark = '';
        this.price = 0;
    }
}
