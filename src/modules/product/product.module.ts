import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductService } from 'src/modules/product/service/product.service';
import { ProductComponent } from 'src/modules/product/page/product.component';
import { AddProductComponent } from 'src/modules/product/page/add-product/add-product.component';
import { EditProductComponent } from 'src/modules/product/page/edit-product/edit-product.component';
import { NgxsModule } from '@ngxs/store';
import { ProductState } from 'src/modules/product/store/product.state';
// import { ProductBiz } from 'src/modules/product/biz-model/product.biz';
import { ProductSelector } from 'src/modules/product/store/product.selector';
import { RouterModule } from '@angular/router';
import { ProductBiz } from 'src/modules/product/biz-model/product.biz';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ProductComponent,
    AddProductComponent,
    EditProductComponent
  ],
  imports: [
    CommonModule,
    NgxsModule.forFeature([ProductState]),
    RouterModule.forChild([
      { path: '', component: ProductComponent },
      { path: 'new', component: AddProductComponent },
      { path: 'edit/:id', component: EditProductComponent }
    ]),
    ReactiveFormsModule
  ],
  providers: [
    ProductBiz,
    ProductService,
    ProductSelector
  ]
})
export class ProductModule { }
