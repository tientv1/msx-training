import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from 'src/modules/product/model/product.model';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class ProductService {

    constructor(private http: HttpClient) { }

    query(): Observable<Product[]> {
        return this.http.get<Product[]>(`${environment.URL}/product`);
    }

    findOne(id: string): Observable<Product> {
        return this.http.get<Product>(`${environment.URL}/product/${id}`);
    }

    add(product: Product): Observable<Product> {
        return this.http.post<Product>(`${environment.URL}/product`, product);
    }

    update(product: Product): Observable<Product> {
        return this.http.put<Product>(`${environment.URL}/product/${product.id}`, product);
    }

    delete(id: string) {
        return this.http.delete(`${environment.URL}/product/${id}`);
    }
}
