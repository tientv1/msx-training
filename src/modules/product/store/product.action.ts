import { Product } from 'src/modules/product/model/product.model';

export class StoreProduct {
    static readonly type = '[PRODUCT] Store Product';
    constructor(public payload: Product[]) { }
}

export class AddProduct {
    static readonly type = '[PRODUCT] Add Product';
    constructor(public payload: Product) { }
}

export class UpdateProduct {
    static readonly type = '[PRODUCT] Update Product';
    constructor(public payload: Product) { }
}

export class DeleteProduct {
    static readonly type = '[PRODUCT] Delete Product';
    constructor(public payload: string) { }
}
