import { Injectable } from '@angular/core';
import { ProductStateModel, ProductState } from 'src/modules/product/store/product.state';
import { Selector } from '@ngxs/store';

@Injectable()
export class ProductSelector {
    @Selector([ProductState])
    static getProduct$(state: ProductStateModel) {
        return state.products;
    }
}
