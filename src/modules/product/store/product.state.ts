import { Product } from 'src/modules/product/model/product.model';
import { State, Action, StateContext } from '@ngxs/store';
import { StoreProduct, AddProduct, UpdateProduct, DeleteProduct } from 'src/modules/product/store/product.action';
import { cloneDeep } from 'lodash';

export interface ProductStateModel {
    products: Product[];
}

@State<ProductStateModel>({
    name: 'ProductState',
    defaults: {
        products: []
    }
})
export class ProductState {

    @Action(StoreProduct)
    storeProduct(ctx: StateContext<ProductStateModel>, action: StoreProduct) {
        ctx.patchState({
            products: action.payload
        });
    }

    @Action(AddProduct)
    addProduct(ctx: StateContext<ProductStateModel>, action: AddProduct) {
        ctx.patchState({
            products: [...ctx.getState().products, action.payload]
        });
    }

    @Action(UpdateProduct)
    updateProduct(ctx: StateContext<ProductStateModel>, action: UpdateProduct) {
        const products = ctx.getState().products;
        const index = products.findIndex(p => p.id === action.payload.id);
        products[index] = cloneDeep(action.payload);
        ctx.patchState({
            products: [...products]
        });
    }

    @Action(DeleteProduct)
    deleteProduct(ctx: StateContext<ProductStateModel>, action: DeleteProduct) {
        const products = ctx.getState().products;
        const index = products.findIndex(p => p.id === action.payload);
        products.splice(index, 1);

        ctx.patchState({
            products: [...products]
        });
    }

}
