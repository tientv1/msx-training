import { Injectable } from '@angular/core';
import { ProductService } from 'src/modules/product/service/product.service';
import { Store } from '@ngxs/store';
import { StoreProduct, AddProduct, UpdateProduct, DeleteProduct } from 'src/modules/product/store/product.action';
import { Product } from 'src/modules/product/model/product.model';
import { Select } from '@ngxs/store';
import { ProductSelector } from 'src/modules/product/store/product.selector';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class ProductBiz {

    @Select(ProductSelector.getProduct$) readonly getProduct$: Observable<Product[]>;

    constructor(private productService: ProductService, private store: Store, private router: Router) { }

    fetch() {
        this.productService.query().subscribe(products => this.store.dispatch(new StoreProduct(products)));
    }

    findOne(id: string) {
        return this.productService.findOne(id);
    }

    add(product: Product) {
        this.productService.add(product).subscribe(p => {
            this.store.dispatch(new AddProduct(p));
            this.back();
        });
    }

    update(product: Product) {
        this.productService.update(product).subscribe(p => {
            this.store.dispatch(new UpdateProduct(p));
            this.back();
        });
    }

    delete(id: string) {
        this.productService.delete(id).subscribe(() => this.store.dispatch(new DeleteProduct(id)));
    }

    back() {
        this.router.navigate(['product']);
    }

}
