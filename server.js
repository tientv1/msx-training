const express = require('express');

const app = express();

app.use(express.static('./dist/msx-training'));

app.get('/*', function (req, res) {
  res.sendFile('index.html', { root: 'dist/msx-training' }
  );
});

app.listen(process.env.PORT || 8080);